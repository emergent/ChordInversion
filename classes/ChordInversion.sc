// Helper function to quickly generate chord inversions

ChordInversion {

	*invert {
		arg chord, invDeg, scaleSize; // chord = array of numbers (degrees or midinotes), invdeg = degree of inversion, scalesize = number of steps per octave in the scale
		var chordSize, inv; // array.size of the chord, inv = the array of scale degrees of the inverted chord
		var shift; // Array that will be added to the rotated version of the chord array
		chordSize = chord.size;
		invDeg = invDeg % chordSize; // suggested by viewer thinkerror
		shift = Array.fill(chordSize - invDeg, 0) ++ Array.fill(invDeg, scaleSize);

		inv = chord.rotate(invDeg.neg) + shift;
		^inv
	}

}